<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S1 Activity</title>
	</head>
	<body>
		<!-- Activity 1 -->
		<h1>Full Address</h1>
		<p><?php echo getFullAddress("#7 Road 9", "North Daang Hari", "Taguig City", "Metro Manila", "Philippines"); ?></p>
		<p><?php echo getFullAddress("Lot 150", "Central Bicutan", "Taguig City", "Metro Manila", "Philippines"); ?></p>
		
		<!-- Activity 2 -->
		<h1>Letter-Based Grading</h1>
		<p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
		<p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
		<p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>
	</body>
</html>